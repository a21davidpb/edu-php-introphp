<html>
	<body>
		<h1>Tu servidor Web funciona correctamente</h1>

		<h4><?php echo "Tu intérprete de PHP también está haciendo su trabajo"?></h4>

		<h2>Ejemplos a tu disposición</h2>
		<ul>
			<li><a href="01basico.php">01basico.php</a></li>
			<li><a href="02variables.php">02variables.php</a></li>
			<li><a href="03aritmetica.php">03aritmetica.php</a></li>
			<li><a href="04comparacion.php">04comparacion.php</a></li>
			<li><a href="05logica.php">05logica.php</a></li>
			<li><a href="06condicional.php">06condicional.php</a></li>
			<li><a href="07bucles.php">07bucles.php</a></li>
			<li><a href="08impresiones.php">08impresiones.php</a></li>
			<li><a href="09arrays.php">09arrays.php</a></li>
			<li><a href="10funciones.php">10funciones.php</a></li>
			<li><a href="11envioDesdeForm.php">11envioDesdeForm.php</a></li>
			<li><a href="12sesiones.php">12sesiones.php</a></li>
			<li><a href="13referencia.php">13referencia.php</a></li>
		</ul>

		<h2>Ejercicios propuestos</h2>
		<ul>
			<li><a href="ejercicios/e01.php">e01 - Generación de múltiplos</a></li>
			<li><a href="ejercicios/e02.php">e02 - Estructura de datos - Alumnos y Notas</a></li>
			<li><a href="ejercicios/e03.php">e03 - Manejo de sesión y Formulario - Alumnos y Notas </a></li>
		</ul>


	</body>
</html>
