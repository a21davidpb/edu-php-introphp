<?php session_start(); ?>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
	<title>Ejercicio 03</title>
	<style>
		.debug{background-color:lightpink;}
	</style>
</head>

<body>
	<div id="enunciado" style="background-color: lightgray;">
	<h1>Enunciado</h1>
	<p>Crea un html con un formulario en el que aparezcan los siguientes campos:</p>
	<ul>
		<li>Nombre</li>
		<li>Apellido</li>
		<li>Edad</li>
		<li>Nota</li>
	</ul>
	<p>Al enviar este formulario al servidor, se deben cumplir los siguientes requerimientos:</p>
	<ol>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Hay cinco alumnos PRECARGADOS que han sacado diferentes calificaciones a lo largo del curso en diferentes tareas asignadas:
			<ul>
				<li>Eugenio Martínez de 45 años - Notas de tareas: 7, 6, 5, 8, 5, 6, 9, 10</li>
				<li>Marta Carrera de 22 - Notas de tareas: 1, 6, 2, 3, 5, 6, 9, 10, 10, 9</li>
				<li>Nacho Herrera de 25 - Notas de tareas: 3, 4, 2, 4, 6, 7, 9, 10, 3, 7</li>
				<li>Anxo Iglesias de 32 - Notas de tareas: 1, 6, 2, 3'2, 5, 2'2, 4'7, 5'5, 9, 9</li>
				<li>Valentina Iglesias de 30 - Notas de tareas: 9, 7</li>
			</ul>
		</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>La estructura de datos elegida es un array indexado por las claves nombre, apellido, edad, notas para cada alumno. Y un array que agrupe estos alumnos</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Es condición necesaria que toda esta estructura estea referenciada por una única variable para que pueda ser almacenada fácilmente en una variable de sesión</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Haz una lista que enumere los alumnos y sus notas medias.</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Calcula notas medias en función de los rangos de edad [18,29], [30,39], [más de 40]</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Haz una lista de aptos, teniendo en cuenta que la condición de APTO se da si tiene al menos 5 calificaciones y una media mínima de 5 puntos</li>
		<li>Todos los datos son obligatorios y ninguno debe de enviarse vacío.</li>
		<li>La estructura de datos debe almacenarse en la sesión del usuario que realiza las peticiones.</li>
		<li>Si no hay ningún alumno coincidente en nombre y apellido, se añadirá este a la estructura de datos.</li>
		<li>Si hay un alumno coincidente en nombre y apellido, se añadirá a este alumno, la nueva nota enviada por formulario.</li>
		<li>Debe existir un botón en el formulario con el texto 'Matar Sesion' que termine con la sesión y consecuentemente con sus datos.</li>
	</ol>

	</div>

	<h1>Resultado</h1>
<?php
	//Tu solución aquí.

//REUTILIZO LO SIGUIENTE e02.php:-------------------------------------------------------------------------------------------------------------------------------------------------------------


$alumno0=[
		'nombre' => 'Eugenio',
		'apellido' => 'Martínez',
		'edad' => 45,
		'notas' => array(7,6,5,8,5,6,9,10)
];

$alumno1=[
		'nombre' => 'Marta',
		'apellido' => 'Carrera',
		'edad' => 22,
		'notas' => array(1,6,2,3,5,6,9,10,10,9)
];

$alumno2=[
		'nombre' => 'Nacho',
		'apellido' => 'Herrera',
		'edad' => 25,
		'notas' => array(3,4,2,4,6,7,9,10,3,7)
];
$alumno3=[
		'nombre' => 'Anxo',
		'apellido' => 'Iglesias',
		'edad' => 32,
		'notas' => array(1,6,2,3.2,5,2.2,4.7,5.5,9,9)
];
$alumno4=[
		'nombre' => 'Valentina',
		'apellido' => 'Iglesias',
		'edad' => 30,
		'notas' => array(9,7)
];


//agrupo los alumnos todos en un array con for en vez de tenerlos de la siguiente manera:-----------------------------------------------------------------
////////$alumno=[$alumno1,$alumno2,$alumno3,$alumno4,$alumno5];
//ya que si en un futuro hay un nuevo alumno, no hay que modificar todo el codigo
$alumno=[];

//El siguiente fragmento, cuenta el numero de variables $alumno1,$alumno2...
//para poder automatizar el proceso y cada vez que se inserte un alumno nuevo
//no tener que modificar el codigo
$pr=0;
$arrcont = [];
//el siguiente "100" ser el numero de alumnos maximo que podremos meter
for($pr;$pr<100;$pr++){
$counter = ${'alumno'.$pr};
$contado = count($counter);
if ($contado == True){
		$me =1;
		array_push($arrcont, $me);
		$cuentAl=count($arrcont);

}
}
//---------------El fragmento anterior se utiliza aqui "$cuentAl":
$l=0;
//La condicion del for "$l<6" se debería modificar si no existiera el fragmento anterior
for ($l; $l<$cuentAl;$l++){
array_push($alumno, ${'alumno'.$l});
}


//SESION------------------------------------------------------------------------------------------------------------------------------------------------------------------
if($_GET['accion']=="matarSesion"){
 //Clické en el botón salir
 session_destroy();
}

if(session_status() == PHP_SESSION_ACTIVE and existeUsuarioSesion()){
								      //Si la SESION ya existeBIEN

											//echo "<p>Hola de nuevo ".'<b>'.$_GET['nombre']." ".$_GET['apellidos'].'</b>'.'<br>'.'<br>'."Tu id de sesión actual es: ".'<b>'.session_id().'</b>'."</p>".'<BR>';
											echo '<br>'."Tu id de sesión actual es: ".'<b>'.session_id().'</b>'.'<BR>'.'<BR>';
											//la siguiente linea la puse antes abajo de todo, encima del boton matar sesion
											imprimeFormulario();

	//SI SE ENVIAN LOS DATOS VACIOS CUANDO YA LA SESION EXISTE
		 	 if($_GET['nombre']==NULL or $_GET['apellidos']==NULL or $_GET['edad']==NULL or $_GET['nota']==NULL){
		 	 //Si no se rellena bien el formulario:
		 	 	echo '<div style="color:red; border:solid 1px;">Datos incompletos, todos los datos son obligatorios y ninguno debe de enviarse vacío.</div><BR>';
				BotonMatarSesion();
				return;

		 }
	////////////////////////////////////////////////////////////
											//Con la siguientes dos lineas me encargo de que nombre y apellido se ponga en mayuscula,
											//asi no habra distincion entre nombres en minuscula y en mayuscula (david y David o perez y Perez)
											$_GET['nombre'] = ucfirst($_GET['nombre']);
											$_GET['apellidos'] = ucfirst($_GET['apellidos']);

											$_SESSION['nombre'] = $_GET['nombre'];
											$_SESSION['apellidos'] = $_GET['apellidos'];
											$alumno=$_SESSION['alumnos'];
											$contador=count($alumno);

											BotonMatarSesion();
											//AQUI LO QUE QUIERO QUE HAGA SI YA EXISTE LA SESION:
											//Metere los nombres y los apellidos de cada alumno en dos arrays distintos
										 	$s=0;
										 	$arrNomb = [];
										 	$arrApe = [];
										 	for($s;$s<$contador;$s++){
										 			array_push($arrNomb, $_SESSION['alumnos'][$s][nombre]);
										 			array_push($arrApe,  $_SESSION['alumnos'][$s][apellido]);
										  }

											if(in_array($_GET['nombre'],$arrNomb) and in_array($_GET['apellidos'],$arrApe)){
												//echo "EXISTE";
												$s=0;
												for($s;$s<=$contador;$s++){
													if($_GET['nombre']==$_SESSION['alumnos'][$s][nombre] and $_GET['apellidos']==$_SESSION['alumnos'][$s][apellido]){
																	array_push($_SESSION['alumnos'][$s][notas],$_GET['nota']);
													}
												}//FALTA EDITAR LAS NOTAS AQUI
											}else{

												${'alumno'.$contador}=[
												'nombre' => $_GET['nombre'],
												'apellido' => $_GET['apellidos'],
												'edad' => $_GET['edad'],
												'notas' => array($_GET['nota'])
												];

													array_push($_SESSION['alumnos'],${'alumno'.$contador});
											}
											$alumno=$_SESSION['alumnos'];
											//echo print_r($alumno);
								//BIEN1
											//CONTINUAR AQUI:


																	//Contador que cuenta cada alumno, lo creo por si en un futuro hay un alumno nuevo
																	$contadoralumnos = count($alumno);



																	//En el siguiente fragmento, craré un array para cada grupo de notas--------------------------------------------------------------------------------------
																	$m=0;
																	$cr = 0;
																	for ($cr;$cr<$contadoralumnos;$cr++){
																		//$arraymedia.$cr = [];} DE ESTA FORMA NO CONSEGUI CREAR ARRAYS CON FOR
																		$arraymedia[$cr]= [];
																	}


																	//Percorro el array creado para cada grupo de notas de cada alumno

																	for ($m;$m<$contadoralumnos;$m++){

																		foreach ($alumno[$m][notas] as $clave => $valor){
																			//echo $valor.',';
																			array_push($arraymedia[$m], $valor);
																	}
																	}

																	$cuentAl=$contador;

																	// TENGO QUE CALCULAR LAS MEDIAS PARA CADA ARRAY DE NOTAS--------------------------------------------------------------------------------------------------
																	$t=0;
																	$cadaMedia = [];
																	//$cuentAll= $cuentAl+1;
																	for($t;$t<=$contador;$t++){
																		 $sumarray= array_sum($arraymedia[$t]);
																		 //echo ' '.$sumarray.' ';
																		 $cantidad = count($arraymedia[$t]);
																		 $med = $sumarray / $cantidad;

																		 array_push(${'cadaMedia'}, $med);
																	}

																	//PUNTO 4: Recorro el array e imprimo el nombre y las notas de cada alumno y la media-----------------------------------------------------------------------
																	$i=0;
																	echo '<br>';
																	echo "<b>LISTADO DE ALUMNOS Y SUS NOTAS MEDIAS: </b>".'</br>';
																	echo "<dl>";
																	for ($alumno[$i];$i<count($alumno);$i++){
																				 $cont += 1;
																				 echo '<dd><b>'.$cont.'º</b> - '.$alumno[$i][nombre].': '.(floor($cadaMedia[$i]*10)/10)."</br>";
																	}
																	echo "</dl>"."</br>";
								//BIEN2
									//CONTINUAR AQUI:

																	//PUNTO 5: Calcular nota media en funcion de rangos de edad-------------------------------------------------------------------------------------------------

																	echo "<b>NOTA MEDIA SEGUN RANGOS RANGOS DE EDAD:</b>".'</br>'.'</br>';

																	$k=0;
																	$p=0;
																	$q=0;
																	$arr29 =[];
																	$arr39 =[];
																	$arr40 =[];

																	echo '<dd>'."<b>[18,29]</b> => <i>medias:</i> ";
																	for ($alumno[$k];$k<$contadoralumnos;$k++){
																	$edad1 = $alumno[$k][edad];
																	if ($edad1 >= 18 and $edad1 < 29){

																	 //echo $edad1.' ';

																	 echo (floor($cadaMedia[$k]*10)/10).', ';
																	 array_push($arr29,$cadaMedia[$k]);
																	 $cant29= count($arr29);
																	 $sum29=array_sum($arr29);
																	 $med29= $sum29/$cant29;
																	 }
																	}
																	echo "=> <b>Media total por edades:</b> ".(floor($med29*10)/10);

																	echo '<dd>'."<b>[30,39]</b> => <i>medias:</i> ";
																	for ($alumno[$p];$p<$contadoralumnos;$p++){
																	$edad2 = $alumno[$p][edad];
																	if ($edad2 >= 30 and $edad2 < 39){

																	//echo $edad2.' ';
																	echo (floor($cadaMedia[$p]*10)/10).', ';
																	array_push($arr39,$cadaMedia[$p]);
																	$cant39= count($arr39);
																	$sum39=array_sum($arr39);
																	$med39= $sum39/$cant39;
																	}
																	}
																	echo "=> <b>Media total por edades:</b> ".(floor($med39*10)/10);


																	echo '<dd>'."<b>[Más de 40]</b> => <i>medias:</i> ";
																	for ($alumno[$q];$q<$contadoralumnos;$q++){
																	$edad3 = $alumno[$q][edad];

																	if ($edad3 >= 40){

																	//echo $edad3.' ';
																	echo (floor($cadaMedia[$q]*10)/10).', ';
																	array_push($arr40,$cadaMedia[$q]);
																	$cant40= count($arr40);
																	$sum40=array_sum($arr40);
																	$med40= $sum40/$cant40;
																	}
																	}
																	echo "=> <b>Media total por edades:</b> ".(floor($med40*10)/10);
																	echo '</dd>';
																	echo "</br>";

								//BIEN3
										//CONTINUAR AQUI:

										//PUNTO 6: Calcular las notas medias en funcion del rango de edad:-----------------------------------------------------------------------------------------

								 	 echo '</br>'."<b>LISTA DE APTOS</b> <i><font size=1>(TIENEN QUE TENER UNA MEDIA MINIMA DE 5 PUNTOS Y 5 CALIFICACIONES):</font></i>".'</br>';

								 	 echo '<dl>';
								 	 $p =0;
								 	 for($alumno[$p];$p<$contadoralumnos;$p++){
								 	 					echo '<dd>';
								 	 					//Imprimo el nombre por cada alumno y sus notas medias
								 	 					echo $alumno[$p][nombre].': ';
								 	 					echo (floor($cadaMedia[$p]*10)/10);
								 	 					// Cuenta las notas de cada alumno
								 	 					$cuentanotas = count($arraymedia[$p]);
								 	 					//Comparo las notas, si tienen menos de 5 de media y menos de 5 notas:
								 	 					if ($cadaMedia[$p] >= 5 and $cuentanotas >= 5){
								 	 						echo "<b> -> APTO</b>";
								 	 					} else {echo "<b> -> NO APTO</b>";}
								 	 					if ($cuentanotas <5){echo " -> <i>Tiene menos de 5 notas</i>";}
								 	 					echo '</dd>';
								 	 }
								 	 echo '</dl>';
								 	 echo '</dd>';


										return;

}else{
												//SI NO EXISTE LA SESION
									 if($_GET['nombre']!=NULL and $_GET['apellidos']!=NULL and $_GET['edad']!=NULL and $_GET['nota']!=NULL){
										 session_start();
										 //Con la siguientes dos lineas me encargo de que nombre y apellido se ponga en mayuscula,
										 //asi no habra distincion entre nombres en minuscula y en mayuscula (david y David o perez y Perez)
										 $_GET['nombre'] = ucfirst($_GET['nombre']);
										 $_GET['apellidos'] = ucfirst($_GET['apellidos']);

										 $_SESSION['nombre'] = $_GET['nombre'];
										 $_SESSION['apellidos'] = $_GET['apellidos'];
										 $_SESSION['alumnos']= $alumno;
										 $cuentaAlu=count($_SESSION['alumnos']);
										 //echo "Hola ".'<b>'.$_GET['nombre']." ".$_GET['apellidos'].'</b>'.'<BR>'.'<BR>'."Su Id de sesion es: ".'<b>'.session_id().'</b>'.'<br>'.'<BR>';
										 echo '<br>'."Tu id de sesión actual es: ".'<b>'.session_id().'</b>'.'<BR>'.'<BR>';
																	 //comprobar si existe el existe el usuario:
																	 //Metere los nombres y los apellidos de cada alumno en dos arrays distintos
																	  $j=0;
																	  $arrNomb = [];
																	  $arrApe = [];
																	  for($j;$j<$cuentaAlu;$j++){
																	 			array_push($arrNomb, $_SESSION['alumnos'][$j][nombre]);
																	 			array_push($arrApe,  $_SESSION['alumnos'][$j][apellido]);
																	 }
																	 //-----
																	 //Compruebo si existen los alumnos por Nombre y Apellidos
																	 if(in_array($_GET['nombre'],$arrNomb) and in_array($_GET['apellidos'],$arrApe)){
																	 		//echo "Existe".'<br>';
																	 		//Si existe, debo añadir sus notas al array existente
																	 					$s=0;
																	 					for($s;$s<=$cuentaAlu;$s++){
																	 						if($_GET['nombre']==$_SESSION['alumnos'][$s][nombre] and $_GET['apellidos']==$_SESSION['alumnos'][$s][apellido]){
																	 										array_push($_SESSION['alumnos'][$s][notas],$_GET['nota']);
																	 						}
																	 					}

																	 	}else{


																			${'alumno'.$cuentaAlu}=[
																 		//$alumno6 =[
																 			'nombre' => $_GET['nombre'],
																 			'apellido' => $_GET['apellidos'],
																 			'edad' => $_GET['edad'],
																 			'notas' => array($_GET['nota'])
																 		];
																 		array_push($_SESSION['alumnos'], ${'alumno'.$cuentaAlu});
																		}
																		//echo print_r($_SESSION['alumnos']);

																		$alumno=$_SESSION['alumnos'];




										 imprimeFormulario();
										 BotonMatarSesion();

										 //Contador que cuenta cada alumno, lo creo por si en un futuro hay un alumno nuevo
										 $contadoralumnos = count($alumno);
										 $cuentaAl=$cuentaAlu;


										 //En el siguiente fragmento, craré un array para cada grupo de notas--------------------------------------------------------------------------------------
										 $m=0;
										 $cr = 0;
										 for ($cr;$cr<$contadoralumnos;$cr++){
										 	//$arraymedia.$cr = [];} DE ESTA FORMA NO CONSEGUI CREAR ARRAYS CON FOR
										 	$arraymedia[$cr]= [];
										 }


										 //Percorro el array creado para cada grupo de notas de cada alumno

										 for ($m;$m<$contadoralumnos;$m++){

										 	foreach ($alumno[$m][notas] as $clave => $valor){
										 		//echo $valor.',';
										 		array_push($arraymedia[$m], $valor);
										 }
										 }


										 			//En este punto tengo varios arrays creados con las notas para cada alumno
										 						/*echo $arraymedia0;
										 							echo $arraymedia1;
										 							...
										 						*/



										 // TENGO QUE CALCULAR LAS MEDIAS PARA CADA ARRAY DE NOTAS--------------------------------------------------------------------------------------------------
										 $t=0;
										 $cadaMedia = [];
										 $cuentAl= $cuentAl+1;
										 for($t;$t<$cuentAl;$t++){
										 		$sumarray= array_sum($arraymedia[$t]);
										 		$cantidad = count($arraymedia[$t]);
										 		$med = $sumarray / $cantidad;

										 		array_push(${'cadaMedia'}, $med);
										 }

										 //PUNTO 4: Recorro el array e imprimo el nombre y las notas de cada alumno y la media-----------------------------------------------------------------------
										 $i=0;
										 echo "<b>LISTADO DE ALUMNOS Y SUS NOTAS MEDIAS: </b>".'</br>';
										 echo "<dl>";
										 for ($alumno[$i];$i<count($alumno);$i++){
										 				$cont += 1;
										 				echo '<dd><b>'.$cont.'º</b> - '.$alumno[$i][nombre].': '.(floor($cadaMedia[$i]*10)/10)."</br>";
										 }
										 echo "</dl>"."</br>";

										 //PUNTO 5: Calcular nota media en funcion de rangos de edad-------------------------------------------------------------------------------------------------

										 echo "<b>NOTA MEDIA SEGUN RANGOS RANGOS DE EDAD:</b>".'</br>'.'</br>';

										 $k=0;
										 $p=0;
										 $q=0;
										 $arr29 =[];
										 $arr39 =[];
										 $arr40 =[];

										 echo '<dd>'."<b>[18,29]</b> => <i>medias:</i> ";
										 for ($alumno[$k];$k<$contadoralumnos;$k++){
										 $edad1 = $alumno[$k][edad];
										 if ($edad1 >= 18 and $edad1 < 29){

										 	//echo $edad1.' ';

										 	echo (floor($cadaMedia[$k]*10)/10).', ';
										 	array_push($arr29,$cadaMedia[$k]);
										 	$cant29= count($arr29);
										 	$sum29=array_sum($arr29);
										 	$med29= $sum29/$cant29;
										 	}
										 }
										 echo "=> <b>Media total por edades:</b> ".(floor($med29*10)/10);

										 echo '<dd>'."<b>[30,39]</b> => <i>medias:</i> ";
										 for ($alumno[$p];$p<$contadoralumnos;$p++){
										 $edad2 = $alumno[$p][edad];
										 if ($edad2 >= 30 and $edad2 < 39){

										 //echo $edad2.' ';
										 echo (floor($cadaMedia[$p]*10)/10).', ';
										 array_push($arr39,$cadaMedia[$p]);
										 $cant39= count($arr39);
										 $sum39=array_sum($arr39);
										 $med39= $sum39/$cant39;
										 }
										 }
										 echo "=> <b>Media total por edades:</b> ".(floor($med39*10)/10);


										 echo '<dd>'."<b>[Más de 40]</b> => <i>medias:</i> ";
										 for ($alumno[$q];$q<$contadoralumnos;$q++){
										 $edad3 = $alumno[$q][edad];

										 if ($edad3 >= 40){

										 //echo $edad3.' ';
										 echo (floor($cadaMedia[$q]*10)/10).', ';
										 array_push($arr40,$cadaMedia[$q]);
										 $cant40= count($arr40);
										 $sum40=array_sum($arr40);
										 $med40= $sum40/$cant40;
										 }
										 }
										 echo "=> <b>Media total por edades:</b> ".(floor($med40*10)/10);
										 echo '</dd>';
										 echo "</br>";

										 //PUNTO 6: Calcular las notas medias en funcion del rango de edad:-----------------------------------------------------------------------------------------

										 echo '</br>'."<b>LISTA DE APTOS</b> <i><font size=1>(TIENEN QUE TENER UNA MEDIA MINIMA DE 5 PUNTOS Y 5 CALIFICACIONES):</font></i>".'</br>';

										 echo '<dl>';
										 $p =0;
										 for($alumno[$p];$p<$contadoralumnos;$p++){
										 					echo '<dd>';
										 					//Imprimo el nombre por cada alumno y sus notas medias
										 					echo $alumno[$p][nombre].': ';
										 					echo (floor($cadaMedia[$p]*10)/10);
										 					// Cuenta las notas de cada alumno
										 					$cuentanotas = count($arraymedia[$p]);
										 					//Comparo las notas, si tienen menos de 5 de media y menos de 5 notas:
										 					if ($cadaMedia[$p] >= 5 and $cuentanotas >= 5){
										 						echo "<b> -> APTO</b>";
										 					} else {echo "<b> -> NO APTO</b>";}
										 					if ($cuentanotas <5){echo " -> <i>Tiene menos de 5 notas</i>";}
										 					echo '</dd>';
										 }
										 echo '</dl>';
										 echo '</dd>';


										 return;
 }else{
	 	if($_GET['nombre']!=NULL or $_GET['apellidos']!=NULL or $_GET['edad']!=NULL or $_GET['nota']!=NULL){
		//Si no se rellena bien el formulario:
	  echo '<div style="color:red; border:solid 1px;">Datos incompletos, todos los datos son obligatorios y ninguno debe de enviarse vacío.</div><BR>';
 }
 	imprimeFormulario();
	//BotonMatarSesion();
}
}

///////////////////////////////////////////
///------------FUNCIONES---------------////
///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function existeUsuarioSesion(){
 return $_SESSION['nombre']!=NULL and $_SESSION['apellidos']!=NULL;
}

function imprimeFormulario(){
	echo '<FORM ACTION="e03.php" METHOD="GET">
						 <b><font size=4>FORMULARIO:</font></b> <BR><dl><dd>
						 Introduce tu nombre: <INPUT TYPE="text" pattern="[a-zA-Z]{1,}$" NAME="nombre" size="20" ><BR><BR>
						 Introduce tu primer apellido: <INPUT TYPE="text" pattern="[a-zA-ZáéíóúÁÉÍÓÚ]{1,}$" NAME="apellidos" size="20"><BR><BR>
						 Introduce tu edad: <INPUT TYPE="number" NAME="edad" min="18" max="100" Style="width:5cm;height:15px"><BR><BR>
						 Introduce tu nota: <INPUT TYPE="number" NAME="nota" min="0" step="0.1" max="10" Style="width:5cm;height:15px"> <i><font size=2>(Entre 0 y 10)</font></i><BR><BR>

					 <INPUT TYPE="submit" VALUE="Enviar">
					 </dd></dl>
		 </FORM> ';
}
function BotonMatarSesion(){
 echo '<div style="float:left;"><FORM ACTION="e03.php" METHOD="GET">
		 <INPUT TYPE="hidden" NAME="accion" VALUE="matarSesion">
		 <INPUT TYPE="submit" VALUE="Matar Sesion">
	 </FORM></div></br></br><BR>';
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//REUTILIZADO DEL e02.php:

//Contador que cuenta cada alumno, lo creo por si en un futuro hay un alumno nuevo
$contadoralumnos = count($alumno);



//En el siguiente fragmento, craré un array para cada grupo de notas--------------------------------------------------------------------------------------
$m=0;
$cr = 0;
for ($cr;$cr<$contadoralumnos;$cr++){
	//$arraymedia.$cr = [];} DE ESTA FORMA NO CONSEGUI CREAR ARRAYS CON FOR
	${'arraymedia'.$cr}= [];
}


//Percorro el array creado para cada grupo de notas de cada alumno

for ($m;$m<$contadoralumnos;$m++){

	foreach (${'alumno'.$m}[notas] as $clave => $valor){
		//echo $valor.',';
		array_push(${'arraymedia'.$m}, $valor);
}
}


			//En este punto tengo varios arrays creados con las notas para cada alumno
						/*echo $arraymedia0;
							echo $arraymedia1;
							...
						*/



// TENGO QUE CALCULAR LAS MEDIAS PARA CADA ARRAY DE NOTAS--------------------------------------------------------------------------------------------------
$t=0;
$cadaMedia = [];
$cuentAl= $cuentAl+1;
for($t;$t<$cuentAl;$t++){
		$sumarray= array_sum(${'arraymedia'.$t});
		$cantidad = count(${'arraymedia'.$t});
		$med = $sumarray / $cantidad;

		array_push(${'cadaMedia'}, $med);
}

//PUNTO 4: Recorro el array e imprimo el nombre y las notas de cada alumno y la media-----------------------------------------------------------------------
$i=0;
echo "<b>LISTADO DE ALUMNOS Y SUS NOTAS MEDIAS: </b>".'</br>';
echo "<dl>";
for ($alumno[$i];$i<count($alumno);$i++){
				$cont += 1;
				echo '<dd><b>'.$cont.'º</b> - '.$alumno[$i][nombre].': '.(floor($cadaMedia[$i]*10)/10)."</br>";
}
echo "</dl>"."</br>";

//PUNTO 5: Calcular nota media en funcion de rangos de edad-------------------------------------------------------------------------------------------------

echo "<b>NOTA MEDIA SEGUN RANGOS RANGOS DE EDAD:</b>".'</br>'.'</br>';

$k=0;
$p=0;
$q=0;
$arr29 =[];
$arr39 =[];
$arr40 =[];

echo '<dd>'."<b>[18,29]</b> => <i>medias:</i> ";
for ($alumno[$k];$k<$contadoralumnos;$k++){
$edad1 = $alumno[$k][edad];
if ($edad1 >= 18 and $edad1 < 29){

	//echo $edad1.' ';

	echo (floor($cadaMedia[$k]*10)/10).', ';
	array_push($arr29,$cadaMedia[$k]);
	$cant29= count($arr29);
	$sum29=array_sum($arr29);
	$med29= $sum29/$cant29;
	}
}
echo "=> <b>Media total por edades:</b> ".(floor($med29*10)/10);

echo '<dd>'."<b>[30,39]</b> => <i>medias:</i> ";
for ($alumno[$p];$p<$contadoralumnos;$p++){
$edad2 = $alumno[$p][edad];
if ($edad2 >= 30 and $edad2 < 39){

//echo $edad2.' ';
echo (floor($cadaMedia[$p]*100)/100).', ';
array_push($arr39,$cadaMedia[$p]);
$cant39= count($arr39);
$sum39=array_sum($arr39);
$med39= $sum39/$cant39;
}
}
echo "=> <b>Media total por edades:</b> ".(floor($med39*10)/10);


echo '<dd>'."<b>[Más de 40]</b> => <i>medias:</i> ";
for ($alumno[$q];$q<$contadoralumnos;$q++){
$edad3 = $alumno[$q][edad];

if ($edad3 >= 40){

//echo $edad3.' ';
echo (floor($cadaMedia[$q]*100)/100).', ';
array_push($arr40,$cadaMedia[$q]);
$cant40= count($arr40);
$sum40=array_sum($arr40);
$med40= $sum40/$cant40;
}
}
echo "=> <b>Media total por edades:</b> ".(floor($med40*10)/10);
echo '</dd>';
echo "</br>";

//PUNTO 6: LISTA DE APTOS:-----------------------------------------------------------------------------------------

echo '</br>'."<b>LISTA DE APTOS</b> <i><font size=1>(TIENEN QUE TENER UNA MEDIA MINIMA DE 5 PUNTOS Y 5 CALIFICACIONES):</font></i>".'</br>';

echo '<dl>';
$p =0;
for($alumno[$p];$p<$contadoralumnos;$p++){
					echo '<dd>';
					//Imprimo el nombre por cada alumno y sus notas medias
					echo $alumno[$p][nombre].': ';
					echo (floor($cadaMedia[$p]*10)/10);
					// Cuenta las notas de cada alumno
					$cuentanotas = count(${'arraymedia'.$p});
					//Comparo las notas, si tienen menos de 5 de media y menos de 5 notas:
					if ($cadaMedia[$p] >= 5 and $cuentanotas >= 5){
						echo "<b> -> APTO</b>";
					} else {echo "<b> -> NO APTO</b>";}
					if ($cuentanotas <5){echo " -> <i>Tiene menos de 5 notas</i>";}
					echo '</dd>';
}
echo '</dl>';
echo '</dd>';


?>

</body>

<html>
