#Changelog#

Puedes y debes alternar este fichero conforme vayas generando versiones:

| Versión  		| changelog  				| Observaciones |
|:------------- 	|:---------------		| :-------------|
| `e01-v1.0`    	| algún texto prolijo 	| opcional      |
| `e01-v1.0-davidPerez`    	| primera version del e01 	| opcional      |
| `e02-v1.0-davidPerez`    	| primera version del e02 	| opcional      |
| `e01-v1.1-davidPerez`    	| modificado error que imprimía 10000 numeros 	| opcional      |
| `e03-v1.0.0-davidPerez`    	| primera version del e03 	| opcional      |
| `e03-v1.1.0-davidPerez`    	| actualización e03 para que no distinga mayusculas ni minusculas 	| opcional      |


