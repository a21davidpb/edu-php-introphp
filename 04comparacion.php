<html>
<head>
<title>Exemplo de PHP</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
</head>
<body>
<?php

	//Declaración de variables
    $a = 8;
    $b = 3;
    $c = 3;
    
    //Impresión ¿hay operaciones de asignación en las siguientes líneas?
    echo "<p>a = 8</p>";
    echo "<p>b = 3</p>";
    echo "<p>c = 3</p>";
    echo "<p>'a' é igual a 'b' ? ---- ";
    
    // ¿Que imprime? ¿Que tipo de dato es?
    echo $a == $b."</p> ";
    
    echo "<p>'a'' é distinto de 'b' ? ---- ";
    
    // ¿Que imprime? ¿Que tipo de dato es?
    echo $a != $b."</p>";
    echo "<p>'a' é maior que 'b' ? ---- ";
    
    // ¿Que imprime? ¿Que tipo de dato es?
    echo $a > $b."</p>";
    echo "<p>'a' é menor que 'b' ? ---- ";
    
    // ¿Que imprime? ¿Que tipo de dato es?
    echo $a < $b."</p>";
    echo "<p>'a' é maior ou igual a 'c' ? ---- ";
    
    // ¿Que imprime? ¿Que tipo de dato es?
    echo $a >= $c."</p>";
    echo "<p>'b' é menor ou igual a 'c' ? ---- ";
    
    // ¿Que imprime? ¿Que tipo de dato es?
    echo $b <= $c."</p>";
    ?>
</body> 
</html>