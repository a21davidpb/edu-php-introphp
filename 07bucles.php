<?php
   
   //En PHP, así como en otros lenguajes podemos comentar todo una línea anteponiendo doble barra

// ---------------------------------------------------------------------------------------   
   //Declaramos una variable asignándole el valor 0
   $i=0; 
   
   //Se ejecutará lo contenido entre los {} del bucle while, mientras se cumpla la condicion de que i sea menor que 10
   while ($i<10) 
   { 
      echo "O valor de i é " . $i . "<br>"; 
      $i++; 
   }
   
   //¿Cuándo valdrá i llegado este punto
   //¿Qué pasaría si iniciasemos la i a 10? ¿Entraría en el while?
   

// ---------------------------------------------------------------------------------------
	$i = 1;
	
	do {
    	echo $i,'<br/>';
    	$i++;
	} while ($i <=10);
	
	//¿Qué hará este código tal y como está?
	//¿Qué sucederá esta vez si declaramos la i a 10?
	
// ---------------------------------------------------------------------------------------

	//Un bucle for, véase la composición de las 3 sentencias dentro de ()
	for($i=0 ; $i<10 ; $i++) 
   	{ 
    	echo "O valor de i é " . $i . "<br />"; 
   	}


?>
