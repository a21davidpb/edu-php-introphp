<html>

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
	<title>Ejercicio 02</title>
</head>

<body>
	<div id="enunciado" style="background-color: lightgray;">
	<h1>Enunciado</h1>
	<p>En un script php debes almacenar en una estructura de datos la siguiente información:</p>
	<ol>
		<li>Hay cinco alumnos que han sacado diferentes calificaciones a lo largo del curso en diferentes tareas asignadas:
			<ul>
				<li>Eugenio Martínez de 45 años - Notas de tareas: 7, 6, 5, 8, 5, 6, 9, 10</li>
				<li>Marta Carrera de 22 - Notas de tareas: 1, 6, 2, 3, 5, 6, 9, 10, 10, 9</li>
				<li>Nacho Herrera de 25 - Notas de tareas: 3, 4, 2, 4, 6, 7, 9, 10, 3, 7</li>
				<li>Anxo Iglesias de 32 - Notas de tareas: 1, 6, 2, 3'2, 5, 2'2, 4'7, 5'5, 9, 9</li>
				<li>Valentina Iglesias de 30 - Notas de tareas: 9, 7</li>
			</ul>
		</li>
		<li>La estructura de datos elegida es un array indexado por las claves nombre, apellido, edad, notas para cada alumno. Y un array que agrupe estos alumnos</li>
		<li>Es condición necesaria que toda esta estructura estea referenciada por una única variable para que pueda ser almacenada fácilmente en una variable de sesión</li>
		<li>Haz una lista que enumere los alumnos y sus notas medias.</li>
		<li>Calcula notas medias en función de los rangos de edad [18,29], [30,39], [más de 40]</li>
		<li>Haz una lista de aptos, teniendo en cuenta que la condición de APTO se da si tiene al menos 5 calificaciones y una media mínima de 5 puntos</li>
	</ol>

	<p>Es necesario que esta sea entregada en plazo para la evaluación</p>

	</div>

	<h1>Resultado</h1>
<?php
//Tu solución aquí
//Arrays para cada alumno (Meti las notas dentro de un array,
//de esta forma si modifico las notas, el programa sigue calculando todo)
		$alumno0=[
				'nombre' => 'Eugenio',
				'apellido' => 'Martinez',
				'edad' => 45,
				'notas' => array(7,6,5,8,5,6,9,10)
		];

//		echo $alumno1[nombre]." ";
//		echo $alumno1[apellido]." ";
//		echo $alumno1[edad]." ";
//		echo $alumno1[notas]."<br/>";

		$alumno1=[
				'nombre' => 'Marta',
				'apellido' => 'Carrera',
				'edad' => 22,
				'notas' => array(1,6,2,3,5,6,9,10,10,9)
		];

		$alumno2=[
				'nombre' => 'Nacho',
				'apellido' => 'Herrera',
				'edad' => 25,
				'notas' => array(3,4,2,4,6,7,8,10,3,7)
		];
		$alumno3=[
				'nombre' => 'Anxo',
				'apellido' => 'Iglesias',
				'edad' => 32,
				'notas' => array(1,6,2,3.2,5,2.2,4.7,5.5,9,9)
		];
		$alumno4=[
				'nombre' => 'Valentina',
				'apellido' => 'Iglesias',
				'edad' => 30,
				'notas' => array(9,7)
		];
		//Si meto mas alumnos mas de prueba, el programa le calcula todo y los lista
		/*
		$alumno5=[
				'nombre' => 'Jacobo',
				'apellido' => 'Martinez',
				'edad' => 90,
				'notas' => array(9,7,4,3,1)
		];
		$alumno6=[
				'nombre' => 'Antonio',
				'apellido' => 'Alvarez',
				'edad' => 30,
				'notas' => array(9,7,8,8,8)
		];*/


//agrupo los alumnos todos en un array con for en vez de tenerlos de la siguiente manera:-----------------------------------------------------------------
////////$alumno=[$alumno1,$alumno2,$alumno3,$alumno4,$alumno5];
//ya que si en un futuro hay un nuevo alumno, no hay que modificar todo el codigo
$alumno=[];
//El siguiente fragmento, cuenta el numero de variables $alumno1,$alumno2...
//para poder automatizar el proceso y cada vez que se inserte un alumno nuevo
//no tener que modificar el codigo
$pr=0;
$arrcont = [];
//el siguiente "100" ser el numero de alumnos maximo que podremos meter
for($pr;$pr<100;$pr++){
		$counter = ${'alumno'.$pr};
		$contado = count($counter);
		if ($contado == True){
				$me =1;
				array_push($arrcont, $me);
				$cuentAl=count($arrcont);
				//$cuentAl= $cuentAl+1;
		}
}
//---------------El fragmento anterior se utiliza aqui "$cuentAl":
$l=0;
//La condicion del for "$l<6" se debería modificar si no existiera el fragmento anterior
for ($l; $l<$cuentAl;$l++){
array_push($alumno, ${'alumno'.$l});
}


//Contador que cuenta cada alumno, lo creo por si en un futuro hay un alumno nuevo
$contadoralumnos = count($alumno);

//En el siguiente fragmento, craré un array para cada grupo de notas--------------------------------------------------------------------------------------
		$m=0;
		$cr = 0;
		for ($cr;$cr<$contadoralumnos;$cr++){
		 	//$arraymedia.$cr = [];} DE ESTA FORMA NO CONSEGUI CREAR ARRAYS CON FOR
			${'arraymedia'.$cr}= [];
		}
//Percorro el array creado para cada grupo de notas de cada alumno
		for ($m;$m<$contadoralumnos;$m++){

			foreach ($alumno[$m][notas] as $clave => $valor){
				//echo $valor.',';
				array_push(${'arraymedia'.$m}, $valor);
		}
		}
					//En este punto tengo varios arrays creados con las notas para cada alumno
								/*echo $arraymedia0;
									echo $arraymedia1;
									...
								*/

// TENGO QUE CALCULAR LAS MEDIAS PARA CADA ARRAY DE NOTAS--------------------------------------------------------------------------------------------------

		$t=0;
		$cadaMedia = [];
		for($t;$t<$contadoralumnos;$t++){
				$sumarray= array_sum(${'arraymedia'.$t});
				$cantidad = count(${'arraymedia'.$t});
				$med = $sumarray / $cantidad;
				//echo $med.' ';
				array_push($cadaMedia, $med);
		}

//PUNTO 4: Recorro el array e imprimo el nombre y las notas de cada alumno y la media-----------------------------------------------------------------------
		$i=0;
		echo "LISTADO DE ALUMNOS Y SUS NOTAS MEDIAS: ".'</br>';
		echo "<dl>";
		for ($alumno[$i];$i<count($alumno);$i++){
						$cont += 1;
						echo '<dd>'.$cont.'º - '.$alumno[$i][nombre].': '.$cadaMedia[$i]."</br>";
		}
		echo "</dl>"."</br>";

//PUNTO 5: Calcular nota media en funcion de rangos de edad-------------------------------------------------------------------------------------------------

echo "NOTA MEDIA SEGUN RANGOS RANGOS DE EDAD:".'</br>'.'</br>';

$k=0;
$p=0;
$q=0;
$arr29 =[];
$arr39 =[];
$arr40 =[];

echo '<dd>'."[18,29] => medias: ";
for ($alumno[$k];$k<$contadoralumnos;$k++){
		$edad1 = $alumno[$k][edad];
		if ($edad1 >= 18 and $edad1 < 29){

			//echo $edad1.' ';

			echo $cadaMedia[$k].', ';
			array_push($arr29,$cadaMedia[$k]);
			$cant29= count($arr29);
			$sum29=array_sum($arr29);
			$med29= $sum29/$cant29;
			}
}
echo "=> Media total por edades: ".$med29;

echo '<dd>'."[30,39] => medias: ";
for ($alumno[$p];$p<$contadoralumnos;$p++){
		$edad2 = $alumno[$p][edad];
	if ($edad2 >= 30 and $edad2 < 39){

		//echo $edad2.' ';
		echo $cadaMedia[$p].', ';
		array_push($arr39,$cadaMedia[$p]);
		$cant39= count($arr39);
		$sum39=array_sum($arr39);
		$med39= $sum39/$cant39;
	}
}
echo "=> Media total por edades: ".$med39;


echo '<dd>'."[Más de 40] => medias: ";
for ($alumno[$q];$q<$contadoralumnos;$q++){
		$edad3 = $alumno[$q][edad];

	if ($edad3 >= 40){

		//echo $edad3.' ';
		echo $cadaMedia[$q].', ';
		array_push($arr40,$cadaMedia[$q]);
		$cant40= count($arr40);
		$sum40=array_sum($arr40);
		$med40= $sum40/$cant40;
	}
}
echo "=> Media total por edades: ".$med40;
echo '</dd>';
echo "</br>";

//PUNTO 6: Calcular las notas medias en funcion del rango de edad:-----------------------------------------------------------------------------------------

echo '</br>'."LISTA DE APTOS (TIENEN QUE TENER UNA MEDIA MINIMA DE 5 PUNTOS Y 5 CALIFICACIONES):".'</br>';

echo '<dl>';
$p =0;
for($alumno[$p];$p<$contadoralumnos;$p++){
							echo '<dd>';
							//Imprimo el nombre por cada alumno y sus notas medias
							echo $alumno[$p][nombre].': ';
							echo $cadaMedia[$p];
							// Cuenta las notas de cada alumno
							$cuentanotas = count(${'arraymedia'.$p});
							//Comparo las notas, si tienen menos de 5 de media y menos de 5 notas:
							if ($cadaMedia[$p] > 5 and $cuentanotas >= 5){
								echo " -> APTO";
							} else {echo " -> NO APTO";}
							if ($cuentanotas <5){echo " -> Tiene menos de 5 notas";}
}
echo '</dl>'

?>

</body>

<html>
