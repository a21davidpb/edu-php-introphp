<?php session_start(); ?> 
<html> 
<head> 
	<title>Envio de datos PHP</title> 
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
</head> 
<body>
	 <?php
	 	//Consultar: http://stackoverflow.com/questions/8812754/cannot-send-session-cache-limiter-headers-already-sent
	 	if($_GET['accion']=="accionSalir"){
	 		//Clické en el botón salir
	 		session_destroy();
	 	}
	 	if(session_status() == PHP_SESSION_ACTIVE and existeUsuarioAsociadoEnSession()){
	 		//La session ya existe
	 		if($_GET['accion']=="accionPeticion"){
	 			$_SESSION['peticionesDeTuSesion']++;
	 		}
	 		echo "<p>Hola de nuevo ".$_SESSION['nombre']."! Tu id de sesión actual es: ".session_id()." Peticiones que has hecho: ".$_SESSION['peticionesDeTuSesion']."</p>";
	 		imprimeBotonPeticion();
	 		imprimeBotonSalir();
	 	}
	 	else{ 
	 		if($_GET['nombre']!=NULL and $_GET['apellidos']!=NULL){
				session_start();
				$_SESSION['nombre'] = $_GET['nombre'];
				$_SESSION['apellidos'] = $_GET['apellidos'];
				$_SESSION['peticionesDeTuSesion'] = 0;
				echo "<p>Bienvenido ".$_SESSION['nombre']." ".$_SESSION['apellidos']."! Tu id de sesión actual es: ".session_id()."</p>";
				
				imprimeBotonPeticion();
				imprimeBotonSalir();
				return;
			}
			else if($_GET['nombre']!=NULL or $_GET['apellidos']!=NULL){
	 			echo '<div style="color:red; border:solid 1px;">Datos incompletos</div>';
			}
			imprimeFormularioLogin();
	 	}
	 	
	 	
	 	function existeUsuarioAsociadoEnSession(){
	 		return $_SESSION['nombre']!=NULL and $_SESSION['apellidos']!=NULL;
	 	}
	 	
	 	function imprimeFormularioLogin(){
			echo '<FORM ACTION="12sesiones.php" METHOD="GET"> 
					Introduce tu nombre:<INPUT TYPE="text" NAME="nombre"><BR> 
					Introduce tus apellidos:<INPUT TYPE="text" NAME="apellidos"><BR> 
					<INPUT TYPE="submit" VALUE="Enviar">
				</FORM> ';
		}
		function imprimeBotonSalir(){
			echo '<div style="float:left;"><FORM ACTION="12sesiones.php" METHOD="GET"> 
					<INPUT TYPE="hidden" NAME="accion" VALUE="accionSalir">
					<INPUT TYPE="submit" VALUE="Salir">
				</FORM></div> ';
		}
		function imprimeBotonPeticion(){
			echo '<div style="float:left;"><FORM ACTION="12sesiones.php" METHOD="GET"> 
					<INPUT TYPE="hidden" NAME="accion" VALUE="accionPeticion">
					<INPUT TYPE="submit" VALUE="Peticion">
				</FORM></div>';
		}
	?>
</body>
</html>

