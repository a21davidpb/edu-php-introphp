<?php
	echo "Ola Mundo";

	echo "Este texto exténdese 
	por varias liñas. Os saltos de liña 
	tamén se envían";

	echo "Este texto exténdese \npor varias liñas. Os saltos de liña\ntamén se envían.";

	echo "Para escapar caracteres, débese indicar \"deste xeito \".";

	// Pódense empregar variables dentro da sentenza echo
	$saudo = "que tal";
	$despedida = "ata logo";

	echo "Ola, $saúdo"; // Ola, que tal

	// Tamén se poden empregar arrays
	$cadea = array("valor" => "saúdo dende un array");

	echo "Este é un {$cadea['valor']} "; //Este é un saúdo dende un array

	// Se se empregan comiñas simples, móstrase o nome da variable, non o seu valor
	echo 'Ola, $saudo'; // ola, $saudo

	// Se non se engade ningún carácter, tamén é posible empregar echo para mostrar o valor das variables 
	echo $saudo;             // que tal
	echo $saudo,$despedida;  // que talata logo

	// O uso de echo con múltiples parámetros é igual que realizar unha concatenación
	echo 'Esta ', 'cadea ', 'está ', 'construída ', 'con moitos parámetros.', chr(10);
	echo 'Esta ' . 'cadea ' . 'está ' . 'construída ' . 'empregando concatenacion.' . "\n";
 
 	//También se puede imprimir utilizando la siguiente sentencia, formateada. Investiga los diferentes formatos en Internet (%d, $f, etc...)
 	printf("O numero dous con diferentes formatos: %d %f %.2f",2,2,2);
?>
