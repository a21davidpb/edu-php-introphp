<html>

<head>
	<title>Ejercicio 01</title>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
</head>

<body>

	<div id="enunciado" style="background-color: lightgray;">
		<h1>Enunciado</h1>
		<p>Esta primera tarea que os planteo se trata de generar, utilizando PHP, una página
		que contenga una división centrada, de 800px de anchura, separada del margen superior
		por 50px y con background gris. En su interior deben ir listados, separados por comas,
		 los 5000 primeros números múltiplos de 3 o de 5.</p>

		<p>Ejemplo: <code>3,5,6,9,10...</code></p>

		<p>Es necesario que esta sea entregada en plazo para la
		evaluación</p>
	</div>

	<h1>Resultado</h1>

<!--TODO: Tu solución aquí-->
<?php
				echo '<div style="background-color:gray;margin-left:auto;margin-right:auto;
				margin-top:50px;width:800px;">';

//Creo un bucle para ir sumando los numeros de 3 en 3 y de 5 en 5 hasta 5000 numeros, y
//un array para ir metiendo los numeros con el fin de ordenarlos despues.
						$contador = 0;
						$a = 3;
						$b = 5;
						$array = [];


						do {
								$contador = $contador +1;
								//echo $a.','.$b.',';
								array_push($array, $a);
								array_push($array, $b);
								$b = $b + 5;
								$a = $a + 3;
						} while ($contador <=5000);

// con "asort()" ordeno de menor a mayor los elementos del array
// con "array_unique()" elimino los valores repetidos del array
						 asort($array);
						 $array =	array_unique($array);
//Con array_slice() obtengo en un array la cantidad de elementos que desee de un array
						 $array5000=array_slice($array,0,5000);
						 //echo print_r($array5000);


//el siguiente foreach es para imprimir los elementos ordenados del array
//encontre en internet PHP_EOL, que se utiliza para representar el fin de linea en HTML
//y que sea legible el resultado.
						foreach($array5000 as $k=>$v){
							echo $v.','.PHP_EOL;
						}

				echo '</div>';

?>

</body>

</html>
