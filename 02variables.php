<html>
<head>
<title>Exemplo de PHP</title>
</head>
<body>
<?php

	//Declaración de variables, nótese que php es débilmente tipado
    $a = 1;
    $b = 3.34;
    $c = "Hola Mundo";
    
    //Presentación en pantalla del valor de las variables
    echo $a,"<br>",$b,"<br>",$c,"<br>";
    
    //Una operacion
    echo $c / 2;
    
    ?>
</body>
</html>